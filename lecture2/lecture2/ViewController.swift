//
//  ViewController.swift
//  lecture2
//
//  Created by олег on 3/10/19.
//  Copyright © 2019 Oleh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // manhetten(money: 24, percent: 6, startYear: 1826, endYear: 2019)
        // moneyYear(stipend: 700, priceIncrease: 3, period: 10)
        // moneyPeriod(stipend: 1000, priceIncrease: 3)
        arroundNumber(number: 37)
    }
    // Задача 1  Остров Манхэттен был приобретен поселенцами за $24 в 1826 г. Каково было бы в настоящее время состояние их счета, если бы эти 24 доллара были помещены тогда в банк под 6% годового дохода?
    func manhetten(money: Int,
                   percent: Double,
                   startYear: Int,
                   endYear: Int ) {
        var sum = 0.00
        for _ in startYear...endYear {
            let deposit = Double(money) * (percent/100)
            sum += deposit
            //print(sum)
        }
        let accountsMoney = sum + Double(money)
        // узнаем состояние счета с учетом вложений
        print("The state of the settlers' accounts in \(endYear) would be $ \(accountsMoney.rounded())")
    }
    
    // Задача 2 Ежемесячная стипендия студента составляет А гривен, а расходы на проживание превышают ее и составляют B грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3% Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
    func moneyYear(stipend: Int,
                   priceIncrease: Int,
                   period: Int) {
        var cost = 1000.0
        var b = 0.0
        if Int(cost) >= stipend {
            for _ in 1...period {
                let a = (Double(cost) * Double(priceIncrease)/100)
                cost = (cost + a) // узнаем сколько потратим за год с учетом роста цен
                b += cost // узнаем сколько дополнительно надо денег
            }
            let c = b - (Double(period) * Double(stipend)) // узнаем разницу за год
            print("To live \(period) month you need \(b.rounded()) uah which \(c.rounded()) more stipend")
        } else {
            print("Stipend \(stipend) more than cost \(Int(cost))")
        }
    }
    //   Задача 3  У студента имеются накопления 5000 грн. Ежемесячная стипендия составляет 1000 гривен, а расходы на проживание превышают ее и составляют 1500 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3% Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию.
    
    func moneyPeriod(stipend: Int,
                     priceIncrease: Int) {
        var cost = 1500.0
        let deposit = 5000.0
        let a = (Double(cost) * Double(priceIncrease)/100)
        cost = (cost + a)
        while (deposit + Double(stipend)) >= cost {
            
        }
        
        print(cost)
    }
    // Задача 4 вводите любую трехзначную  цело численную переменную типа 256, 341, 712 После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед 652, 143, 217
    func arroundNumber(number: Int) {
        let a = 10
        print(number % a)
        //print(number/a)
        
    }
}

